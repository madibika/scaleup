import React, {useEffect, useState} from "react";
import styles from "../../assets/styles/AdminStyles/QuestionnaireBlocksPage.module.scss"
import GoBack from "../../components/OtherComponents/GoBack";
import Icon1 from '../../assets/icons/mainIcon1.svg'
import Icon2 from '../../assets/icons/mainIcon2.svg'
import Icon3 from '../../assets/icons/mainIcon3.svg'
import Icon4 from '../../assets/icons/mainIcon4.svg'
import QuestionnaireBlockItem from "../../components/AdminComponents/QuestionnaireBlockItem";
import {useDispatch, useSelector} from "react-redux";
import {useParams} from "react-router-dom";
import {getCurrentQuestionnaireAction} from "../../redux/actions/getCurrentQuestionnaireAction";
import Modal from "react-modal";
import CompanyInfoModal from "../../components/AdminComponents/CompanyInfoModal";
import {phoneFormat} from "../../tools/phoneForm";
import {returnDateFormat} from "../../tools/returnDateFormat";
const customStyles = {
    content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)',
        borderRadius          : '32px',
        padding               : 0
    }
};


const QuestionnaireBlocksPage = () => {
    const [modalIsOpen,setOpen] = useState(false);
    const {id} = useParams();
    const dispatch = useDispatch();
    const {currentItemData} = useSelector(state => state.QuestionnairePage);
    const [sections, setSections] = useState([
        {
            icon: Icon1,
            title: "Стратегический раздел",
            count: 20,
            status: "Сохранено",
            percent: 0
        },
        {
            icon: Icon2,
            title: "Финансовый раздел",
            count: 20,
            status: "Сохранено",
            percent: 0
        },
        {
            icon: Icon3,
            title: "Юридический раздел",
            count: 20,
            status: "Сохранено",
            percent: 0
        },
        {
            icon: Icon4,
            title: "Маркетинговый раздел",
            count: 20,
            status: "Сохранено",
            percent: 0
        }
    ]);

    useEffect(() => {
        dispatch(getCurrentQuestionnaireAction(id))
    }, []);

    const handleOpen = () => {
        setOpen(true)
    };

    const closeModal = () => {
        setOpen(false);
    };


    return (
        <>
            {currentItemData && (
                <Modal
                    isOpen={modalIsOpen}
                    onRequestClose={closeModal}
                    style={customStyles}
                    contentLabel="Example Modal"
                >
                    <CompanyInfoModal
                        companyName={currentItemData.user.company}
                        address={null}
                        branch={null}
                        fio={currentItemData.user.fio}
                        phone={phoneFormat(currentItemData.user.phone)}
                        email={currentItemData.user.email}
                        tarif={currentItemData.service.name}
                        registration_date={returnDateFormat(currentItemData.user.created_at)}
                        file={currentItemData.invoice.link}
                        status={currentItemData.payment_status.id}
                        payment_type={currentItemData.payment_type.id}
                        closeModal={closeModal}
                    />
                </Modal>
            )}
            <div className={styles.container}>
                <div className={styles.blocks_container}>
                    <div className={styles.blocks_header}>
                        <GoBack
                            title={currentItemData && currentItemData.user.company}
                            subtitle="Детали компании"
                        />
                        <div onClick={handleOpen} className={styles.info_btn}>
                            Инфо о компании
                        </div>
                    </div>
                    <div className={styles.status_block}>
                        <div className={styles.changeStatus_txt}>
                            Чтобы отправить компанию на след. этап “Произдводство” измените статус “Заполнена” на
                            “Готово”
                        </div>
                        <div className={styles.changeStatus_btn}>
                            Изменить статус
                        </div>
                    </div>
                    <div className={styles.mainPageBlocks}>
                        {sections && sections.map((el, index) => (
                            <QuestionnaireBlockItem
                                key={index}
                                icon={el.icon}
                                count={el.count}
                                status={el.status}
                                percent={el.percent}
                            />
                        ))}
                    </div>
                </div>
            </div>
        </>
    )
};

export default QuestionnaireBlocksPage;
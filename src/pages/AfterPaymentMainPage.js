import React, {useEffect, useState} from "react";
import styles from '../assets/styles/MainStyles/AfterPaymentMainPage.module.scss'
import Icon1 from '../assets/icons/mainIcon1.svg'
import Icon2 from '../assets/icons/mainIcon2.svg'
import Icon3 from '../assets/icons/mainIcon3.svg'
import Icon4 from '../assets/icons/mainIcon4.svg'
import Faq from "../components/FaqComponents/Faq";
import MainItem from "../components/MainComponents/MainItem";
import MainService from "../services/MainService";
import RocketBlock from "../components/FormConponents/RocketBlock";
import AnketaProgress from "../components/FormConponents/AnketaProgress";
import CircularIndeterminate from "../components/FormConponents/ProgressCircle";

const textStyle = {
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 16,
    lineHeight: '22px',
    textAlign: 'center',
    color: '#171717'
};
const AfterPaymentMainPage = () => {
    const [sections,setSections] = useState(null);
    const [sectionTitles] = useState(["strategy","financial","legal","marketing"]);

    useEffect(()=>{
        const response = new MainService().getSections();
        response.then(res=>{
            console.log(res.data);
            setSections(res.data)
        });
        response.catch(err=>{
            console.log(err)
        })
    },[]);

    const [next,setNext] = useState(0);

    useEffect(()=>{
        if (next === 1){
            setTimeout(()=>{
                setNext(2)
            },2000)
        }
    },[next]);

    return(
        <div className={styles.container}>
            <div className={styles.mainPageContainer}>
                <div className={styles.title}>
                    Главная
                </div>
                <div className={styles.subtitle}>
                    Вам необходимо заполнить все анкеты
                </div>
                {next === 0 ? (
                    <div style={{marginTop:32}}>
                        <RocketBlock setNext={setNext}/>
                    </div>
                ):(next === 1 ? (
                    <div style={{marginTop: 32}}>
                        <div style={{width:'100%',height:'330px',background: '#FFFFFF',
                            boxShadow: '0px 10px 20px rgba(41, 41, 42, 0.07)',
                            borderRadius: '24px',display:'flex',justifyContent:'center',alignItems:'center'}}>
                            <div>
                                <CircularIndeterminate/>
                                <div style={textStyle}>Подождите... Идет отправка</div>
                            </div>
                        </div>
                    </div>
                ):(
                    <div style={{marginTop:32}}>
                        <AnketaProgress/>
                    </div>
                ))}
                <div className={styles.mainPageBlocks}>
                    {sections && sections.map((el,index)=>(
                        <MainItem
                            answers_count={el.answers_count}
                            forms_count={el.forms_count}
                            id={el.id}
                            key={el.id}
                            title={el.name}
                            description={el.description}
                            link={el.link}
                            subtitle="? вопросов"
                            sectionTitle={sectionTitles[index]}
                            icon={el.id === 1 ? Icon1 : el.id === 2 ? Icon2 : el.id === 3 ? Icon3 : Icon4}/>
                    ))}
                </div>
            </div>
            <Faq/>
        </div>
    )
};

export default AfterPaymentMainPage;
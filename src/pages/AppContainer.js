import React, {useEffect} from "react";
import AppHeader from "../components/OtherComponents/AppHeader";
import {Route} from 'react-router-dom'
import MainPage from "./MainPage";
import FaqPage from "./FaqPage";
import AppFooter from "../components/OtherComponents/AppFooter";
import PrivacyAgreement from "./PrivacyAgreemnet";
import PaymentPage from "./PaymentPage";
import ApplicationFormPage from "./ApplicationFormPage";
import ProfilePage from "./ProfilePage";
import AdminPage from "./admin/AdminPage";
import QuestionnairePage from "./admin/QuestionnairePage";
import QuestionnaireBlocksPage from "./admin/QuestionnaireBlocksPage";
import AdminProfilePage from "./admin/AdminProfilePage";
import AdminSettingsPage from "./admin/AdminSettingsPage";
import AdminFaqsPage from "./admin/AdminFaqPage";
import WelcomeBlock from "./admin/WelcomeBlock";
import {useHistory} from "react-router-dom";
import styles from "../assets/styles/AppContainer.module.scss";
import MyDocumentsPage from "./MyDocumentsPage";

const AppContainer = ({userData}) => {
    const history = useHistory();

    useEffect(()=>{
        if (userData){
            if (history.location.pathname.includes('/admin') && userData.roles[0].name === 'client'){
                history.push('/')
            }
            if (userData.roles[0].name === "admin" && history.location.pathname.includes('/admin')){
                history.push(history.location.pathname);
            }
            if (userData.roles[0].name === "admin" && !history.location.pathname.includes('/admin') ){
                history.push('/admin')
            }
        }
    },[userData]);

    return (
        <>
            {userData && (
                <div>
                    <AppHeader
                        userData={userData}
                    />
                    {/*<div className={styles.background_top}></div>*/}
                    {/*<div className={styles.background_bottom}></div>*/}
                    <div style={{minHeight: 'calc(100vh - 182px)'}}>
                        <Route exact path={'/profile'} render={() => <ProfilePage/>}/>
                        <Route exact path={'/'} render={() => <MainPage userData={userData}/>}/>
                        <Route exact path={'/faq'} render={() => <FaqPage/>}/>
                        <Route exact path={'/privacyAgreement'} render={() => <PrivacyAgreement/>}/>
                        <Route exact path={'/payment'} render={() => <PaymentPage/>}/>
                        <Route path={'/form'} render={() => <ApplicationFormPage/>}/>
                        <Route exact path={'/admin'} render={() => <AdminPage/>}/>
                        <Route exact path={'/admin/questionnaire'} render={() => <QuestionnairePage/>}/>
                        <Route exact path={'/admin/questionnaire/:id'} render={()=> <QuestionnaireBlocksPage/>}/>
                        <Route exact path={'/admin/profile'} render={()=><AdminProfilePage/>}/>
                        <Route exact path={'/admin/settings'} render={()=><AdminSettingsPage/>}/>
                        <Route exact path={'/admin/settings/faqs'} render={()=> <AdminFaqsPage/>}/>
                        <Route exact path={'/admin/settings/welcome'} render={()=><WelcomeBlock/>}/>
                        <Route exact path={'/my-documents'} render={()=><MyDocumentsPage/>}/>

                    </div>
                    <AppFooter/>
                </div>
            )}
        </>
    )
};

export default AppContainer
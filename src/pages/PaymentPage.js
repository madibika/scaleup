import React, {useEffect, useState} from "react";
import styles from '../assets/styles/PaymentStyles/PaymentPage.module.scss'
import GoBack from "../components/OtherComponents/GoBack";
import Faq from "../components/FaqComponents/Faq";
import PaymentBlockTitle from "../components/PaymentComponents/PaymentBlockTitle";
import PaymentChoicePay from "../components/PaymentComponents/PaymentChoicePay";
import PaymentCheckDownload from "../components/PaymentComponents/PaymentCheckDownload";
import PaymentSerivce from "../services/PaymentService";
import {useSelector} from "react-redux";

const PaymentPage = () => {
    const [checkBoxHover,setCheckBoxHover] = useState('');
    const {userData} = useSelector(state => state.AuthPage);
    const [isDownload,setDownload] = useState(userData.payment ? (userData.payment.payment_type_id === 2 ? true : false) :false);
    const [choice,setChoice] = useState('');


    const [paymentInfo,setPaymentInfo] = useState(null);

    useEffect(()=>{
        const response = new PaymentSerivce().userPayments();
        response.then(res=>{
            setPaymentInfo(res.data)
        });
        response.catch(err=>{
            console.log(err)
        })
    },[]);

    return (
        <div className={styles.container}>
            <div className={styles.payment_container}>
                <GoBack path={'/'} title="Страница оплаты" subtitle="Сформирован счет 2021-01-11"/>
                <div className={styles.payment_blocks}>
                    {paymentInfo && (
                        <PaymentBlockTitle paymentInfo={paymentInfo}/>
                    )}
                    {(!isDownload) ? (
                        <PaymentChoicePay
                            choice={choice}
                            setChoice={setChoice}
                            checkBoxHover={checkBoxHover}
                            setCheckBoxHover={setCheckBoxHover}
                            setDownload={setDownload}
                        />
                    ):(
                        <PaymentCheckDownload
                            setDownload={setDownload}
                        />
                    )}
                </div>
            </div>
            <Faq/>
        </div>
    )
};

export default PaymentPage;
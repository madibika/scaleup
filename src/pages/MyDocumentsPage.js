import React, {useState} from "react";
import styles from "../assets/styles/Personal.module.css"
import {ReactComponent as FolderIcon} from "../assets/icons/folder.svg";
import {ReactComponent as XlsIcon} from "../assets/icons/xls.svg";
import {ReactComponent as WordIcon} from "../assets/icons/word.svg";
import {ReactComponent as PdfIcon} from "../assets/icons/pdf.svg";
import {ReactComponent as PptxIcon} from "../assets/icons/pptx.svg";
import {ReactComponent as StrategyIcon} from "../assets/icons/mainIcon1.svg";
import {ReactComponent as FinanceIcon} from "../assets/icons/mainIcon2.svg";
import {ReactComponent as LegalIcon} from "../assets/icons/mainIcon3.svg";
import {ReactComponent as MarketingIcon} from "../assets/icons/mainIcon4.svg";




const MyDocumentsPage = () => {

    const [docsData] = useState([
        {
            name: "Стратегический раздел",
            count: "20 документов",
            icon: <StrategyIcon/>,
            docs:[
                {
                    name:"BrandFiles",
                    description:"8 файлов",
                    icon: <FolderIcon/>
                },
                {
                    name:"FolderName",
                    description:"8 файлов",
                    icon: <FolderIcon/>
                },
                {
                    name:"Job Description.xls",
                    description:"324 kb",
                    icon: <XlsIcon/>
                },
                {
                    name:"Job Description.docx",
                    description:"324 kb",
                    icon: <WordIcon/>
                },
                {
                    name:"Job Description.docx",
                    description:"324 kb",
                    icon: <WordIcon/>
                },
                {
                    name:"Job Description.docx",
                    description:"324 kb",
                    icon: <WordIcon/>
                },
                {
                    name:"Filename.pdf",
                    description:"1324 kb",
                    icon: <PdfIcon/>
                },
                {
                    name:"Brand.pptx",
                    description:"324 kb",
                    icon: <PptxIcon/>
                },
            ]
        },
        {
            name: "Финансовый раздел",
            count: "124 документов",
            icon: <FinanceIcon/>,
            docs:[
                {
                    name:"BrandFiles",
                    description:"8 файлов",
                    icon: <FolderIcon/>
                },
                {
                    name:"FolderName",
                    description:"8 файлов",
                    icon: <FolderIcon/>
                },
                {
                    name:"Job Description.xls",
                    description:"324 kb",
                    icon: <XlsIcon/>
                },
                {
                    name:"Job Description.docx",
                    description:"324 kb",
                    icon: <WordIcon/>
                },
                {
                    name:"Filename.pdf",
                    description:"1324 kb",
                    icon: <PdfIcon/>
                },
                {
                    name:"Brand.pptx",
                    description:"324 kb",
                    icon: <PptxIcon/>
                },
            ]

        },
        {
            name: "Юридический раздел",
            count: "15 документов",
            icon: <LegalIcon/>,
            docs:[
                {
                    name:"BrandFiles",
                    description:"8 файлов",
                    icon: <FolderIcon/>
                },
                {
                    name:"FolderName",
                    description:"8 файлов",
                    icon: <FolderIcon/>
                },
                {
                    name:"Job Description.xls",
                    description:"324 kb",
                    icon: <XlsIcon/>
                },
            ]

        },
        {
            name: "Маркетинговый раздел",
            count: "124 документов",
            icon: <MarketingIcon/>,
            docs:[
                {
                    name:"BrandFiles",
                    description:"8 файлов",
                    icon: <FolderIcon/>
                },
                {
                    name:"FolderName",
                    description:"8 файлов",
                    icon: <FolderIcon/>
                },
                {
                    name:"Job Description.xls",
                    description:"324 kb",
                    icon: <XlsIcon/>
                },
                {
                    name:"Filename.pdf",
                    description:"1324 kb",
                    icon: <PdfIcon/>
                },
                {
                    name:"Brand.pptx",
                    description:"324 kb",
                    icon: <PptxIcon/>
                },
            ]

        }

    ]);


    return (
        <div className={styles.fake_container}>
            <div className={styles.fake_wrappers}>
                {docsData.map((el,index)=>(
                    <div key={index} className={styles.four_containers}>
                        <div className={styles.doc_heading}>
                            <div className={styles.svg_leftside}>
                                {/*<div className={styles.pinky}></div>*/}
                                {el.icon}
                            </div>
                            <div className={styles.svg_rightside}>
                                <div className={styles.heading_name}>
                                    {el.name}
                                </div>
                                <div className={styles.count}>
                                    {el.count}
                                </div>
                            </div>
                        </div>
                        <div className={styles.main_docs}>
                            {el.docs.map((el,index)=>(
                                <div key={index} className={styles.document}>
                                    <div className={styles.doc_svg_leftside}>
                                        {el.icon}
                                    </div>
                                    <div className={styles.doc_svg_rightside}>
                                        <div className={styles.doc_name}>
                                            {el.name}
                                        </div>
                                        <div className={styles.doc_count}>
                                            {el.description}
                                        </div>
                                    </div>
                                </div>
                            ))}
                        </div>
                    </div>
                ))}
            </div>


        </div>
    )
};
export default MyDocumentsPage;
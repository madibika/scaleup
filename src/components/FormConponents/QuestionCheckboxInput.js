import React, {useEffect, useState} from "react";
import styles from "../../assets/styles/FormStyles/QuestionCheckboxInput.module.scss";
import AnketaService from "../../services/AnketaService";
import TextField from "@material-ui/core/TextField/TextField";

const QuestionCheckboxInput = ({id,title,text,require,questionsData,getStrategyQuestion}) => {

    const [answers,setAnswers] = useState(null);
    const [formDisabled,setFormDisabled] = useState(true);
    const [formAnswer,setFormAnswer] = useState('');
    const [focus,setFocus] = useState(false);

    console.log(questionsData);

    useEffect(()=>{
            let obj = {};
            let drugoe = "";
            for (const [key, value] of Object.entries(questionsData.answers)) {
                console.log(value);
                if ( value === "Другое:"){
                    drugoe = key;
                }
                obj[key] = {
                    [key]: value,
                    checked : false
                };
            }
            if (questionsData.answer){
                let str = "ans-";
                for (const [key, value] of Object.entries(questionsData.answer.answers)) {
                    console.log(str+key);
                    console.log(drugoe);
                    if ((str+key) === drugoe){
                        setFormAnswer(value);
                        setFormDisabled(false);
                        obj[str+key] = {
                            [str+key]: "Другое:",
                            checked : true
                        }
                    }else{
                        obj[str+key] = {
                            [str+key]: value,
                            checked : true
                        };
                    }
                }
            }
            setAnswers(obj);

    },[questionsData]);

    const handleChange = (name,bool=false) => {

        const copy = {...answers};
        if (!bool){
            copy[name].checked = !copy[name].checked;
        }
        setAnswers(copy);
        let obj = {};
        let i=1;
        for (const [key, value] of Object.entries(copy)) {
            if (value[key] === "Другое:"){
                if (copy[key].checked){
                    obj[i] = formAnswer;
                }
            }else{
                if (copy[key].checked){
                    obj[i] = value[key];
                }
            }
            i++;
        }
        const response = new AnketaService().saveForm(id,obj,4);
        response.then(res=>{
            console.log(res);
            getStrategyQuestion();
        })
    };



    return(
        <div className={styles.root}>
            {title && (
                <div className={styles.title}>
                    {title}
                </div>
            )}
            <div className={styles.text}>
                {text} {require && (
                <span>*</span>
            )}
            </div>
            <div className={styles.input}>
                {answers && (
                    <>
                        {Object.keys(answers).map((key,index)=>(
                            <div key={index} className={styles.checkbox}>
                                {answers[key][key] === "Другое:" ? (
                                    <div className={styles.another}>
                                        <input
                                            onChange={(e)=>{
                                            handleChange(key);
                                            setFormDisabled(!formDisabled);
                                            if (!formDisabled === true){
                                                setFormAnswer('');
                                            }
                                        }} checked={answers[key].checked} type="checkbox"/>
                                        <label>{answers[key][key]}</label><br/>
                                        <TextField
                                            className={focus || formAnswer ? `${styles.textField} on` : `${styles.textField} off`}
                                            id="outlined-basic"
                                            label={"Мой ответ"}
                                            variant="outlined"
                                            disabled={formDisabled}
                                            value={formAnswer ? formAnswer : ""}
                                            onFocus={()=>{
                                                setFocus(true);
                                            }}
                                            onBlur={()=>{
                                                setFocus('');
                                                handleChange(key,true);
                                            }}
                                            onChange={(e)=>{
                                                setFormAnswer(e.target.value);
                                            }}
                                        />
                                    </div>
                                ):(
                                    <>
                                        <input
                                            disabled={questionsData ? (questionsData.answer ? (questionsData.answer.disabled === 1 ) : false) : false}
                                            onChange={(e)=>handleChange(key)}  checked={answers[key].checked} type="checkbox"/>
                                        <label>{answers[key][key]}</label><br/>
                                    </>
                                )}
                            </div>
                        ))}
                    </>
                )}
            </div>
        </div>
    )
};
export default QuestionCheckboxInput;
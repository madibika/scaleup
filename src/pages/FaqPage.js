import React, {useEffect, useState} from "react";
import styles from "../assets/styles/FaqStyles/FaqPage.module.scss"
import GoBack from "../components/OtherComponents/GoBack";
import FaqList from "../components/FaqComponents/FaqList";
import {useDispatch, useSelector} from "react-redux";
import {getJustFaqsAction} from "../redux/actions/getJustFaqsAction";


const FaqPage = () => {
    const dispatch = useDispatch();
    // const {faqData} = useSelector(state => state.MainPage);
    const [allData,setAllData] = useState(null);

    const [currentData,setCurrentData] = useState( allData ? allData[0].faqs[0] : null);


    useEffect(()=>{
       const response = dispatch(getJustFaqsAction());
       response.then(res=>{
           setAllData(res.data);
           setCurrentData(res.data[0]);
           console.log(res)
       }).catch(err=>{
           console.log(err)
       })
    },[]);

    const openQuestionsHandleChange = id => {
        for (let i=0;i<allData.length;i++){
            if(allData[i].id === id){
                setCurrentData(allData[i])
            }
        }
    };

    return(
        <div className={styles.container}>
            <div className={styles.faqPage_container}>
                <GoBack title={"Часто задаваемые вопросы"}/>
                <div className={styles.faqPage_block}>
                    {allData && (
                        <>
                            <div className={styles.section_block}>
                                {allData.map((el,index)=>(
                                    <div
                                        style={currentData && currentData.id === el.id ? {fontWeight:'bold',fontSize:16,color:'#000'} : {}}
                                         onClick={openQuestionsHandleChange.bind(this,el.id)} className={styles.section_title} key={index}>{el.title}</div>
                                ))}
                            </div>
                            {currentData && (
                                <div className={styles.questions_block}>
                                    <FaqList faqData={currentData.faqs}/>
                                </div>
                            )}
                        </>
                    )}
                </div>
            </div>
        </div>
    )
};

export default FaqPage
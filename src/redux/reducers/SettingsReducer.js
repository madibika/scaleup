import {
    SERVICES_DATA_ERROR,
    SERVICES_DATA_PENDING, SERVICES_DATA_SUCCESS, USERS_LIST_ERROR, USERS_LIST_PENDING, USERS_LIST_SUCCESS,
    WORKERS_DATA_ERROR,
    WORKERS_DATA_PENDING,
    WORKERS_DATA_SUCCESS
} from "../../types/AdminTypes";
import {ERROR, PENDING, SUCCESS} from "../../types/types";


const initialState = {
    workers:null,
    workers_status:'',
    services: null,
    services_status:'',
    users_list: null,
    users_list_status:''
};

export const SettingsReducer = (state=initialState,action) => {
    switch (action.type) {
        case WORKERS_DATA_PENDING:{
            return{
                ...state,
                workers_status: PENDING
            }
        }
        case WORKERS_DATA_SUCCESS:{
            return {
                ...state,
                workers_status: SUCCESS,
                workers: action.payload
            }
        }
        case WORKERS_DATA_ERROR:{
            return {
                ...state,
                workers_status: ERROR
            }
        }
        case SERVICES_DATA_PENDING:{
            return {
                ...state,
                services_status: PENDING
            }
        }
        case SERVICES_DATA_SUCCESS:{
            return {
                ...state,
                services: action.payload,
                services_status: SUCCESS
            }
        }
        case SERVICES_DATA_ERROR:{
            return {
                ...state,
                services_status: ERROR
            }
        }
        case USERS_LIST_PENDING:{
            return {
                ...state,
                users_list_status: PENDING
            }
        }
        case USERS_LIST_SUCCESS:{
            return {
                ...state,
                users_list_status: SUCCESS,
                users_list: action.payload
            }
        }
        case USERS_LIST_ERROR:{
            return {
                ...state,
                users_list_status: ERROR
            }
        }
        default:
            return state;
    }
};
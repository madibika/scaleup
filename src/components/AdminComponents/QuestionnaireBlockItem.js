import React from "react";
import styles from "../../assets/styles/AdminStyles/QuestionnaireBlockItem.module.scss"
import ProgressLine from "../FormConponents/ProgressLine";

const QuestionnaireBlockItem = ({icon,count,status,percent}) => {
    return(
        <div className={styles.container}>
            <div className={styles.header}>
                <div className={styles.icon}>
                    <img src={icon} alt=""/>
                </div>
                <div className={styles.info}>
                    <div className={styles.title}>Стратегический раздел</div>
                    <div className={styles.subtitle}>
                        <div className={styles.subtitle_title}>20 вопросов</div>
                        <div className={styles.status}>
                            <div className={styles.circle}></div>
                            <div className={styles.status_text}>Сохранена</div>
                        </div>
                    </div>
                </div>
            </div>
            <div className={styles.progressText}>
                Заполнено 0%
            </div>
            <ProgressLine/>
            <div  className={styles.block_btn}>
                Посмотреть анкету
            </div>
        </div>
    )
};
export default QuestionnaireBlockItem;
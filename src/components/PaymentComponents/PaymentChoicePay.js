import React, {useState} from "react";
import styles from "../../assets/styles/PaymentStyles/PaymentPage.module.scss";
import MasterCard from "../../assets/icons/mastercard.svg";
import Visa from "../../assets/icons/visa.svg";
import CreditCard from "../../assets/icons/creditcard.svg";
import CashCard from "../../assets/icons/cashcard.svg";
import axios from '../../plugins/axios'
import PaymentSerivce from "../../services/PaymentService";

const PaymentChoicePay = ({choice,setChoice,checkBoxHover,setCheckBoxHover,setDownload}) => {
console.log(choice)
    const checkDownload = () => {
        setDownload(true);
        // window.open(axios.defaults.baseURL+'/payments/invoice-file','_self')
        const response = new PaymentSerivce().paymentSelect(2);
        response.then(res=>{
            console.log(res)
        });
        response.catch(err=>{
            console.log(err)
        });
    };

    return(
        <div className={styles.payment_block_body}>
            <div className={styles.payment_block_body_title}><h3>Выберите способ оплаты</h3></div>
            <div className={styles.payment_block_body_choice}>
                <div
                    style={choice === 1 ? {backgroundColor: '#F3F6F8',transition:'.4s'} : {}}
                    onClick={setChoice.bind(this, 1)}
                    onMouseEnter={()=>setCheckBoxHover( 1)}
                    onMouseLeave={()=>setCheckBoxHover( '')}
                    className={styles.bank_card}>
                    <div className={styles.bank_card_visa_mastercard}>
                        <img src={MasterCard} alt="mastercard"/>
                        <img src={Visa} alt="visa"/>
                    </div>
                    {(checkBoxHover === 1 || choice ===1 ) ? (
                        <div className={styles.checkbox}>
                            <input checked={choice===1 ? true : false} onClick={setChoice.bind(this, 1)}  type="checkbox" id="vehicle1" name="bankcard" value=""/>
                        </div>
                    ):(
                        <div></div>
                    )}
                    <div className={styles.bank_card_body}>
                        <img src={CreditCard} alt="creditcard"/>
                        <div>Банковская карта</div>
                    </div>

                </div>
                <div
                    style={choice === 2 ? {backgroundColor: '#F3F6F8',transition:'.4s'} : {}}
                    onClick={setChoice.bind(this, 2)}
                    onMouseEnter={()=>setCheckBoxHover(2)}
                    onMouseLeave={()=>setCheckBoxHover('')}
                    className={styles.bank_card}>
                    <div className={styles.bank_card_body}>
                        <img src={CashCard} alt="cashcard"/>
                        <div>Банковский перевод</div>
                    </div>
                    {(checkBoxHover === 2 || choice===2) ? (
                        <div className={styles.checkbox}>
                            <input checked={choice===2 ? true : false} onClick={setChoice.bind(this, 2)}  type="checkbox" id="vehicle1" name="cashcard" value=""/>
                        </div>
                    ):(
                        <div></div>
                    )}
                </div>
            </div>
            <div onClick={checkDownload} className={styles.payment_button}>
                Скачать счет на оплату
            </div>
        </div>
    )
};

export default PaymentChoicePay
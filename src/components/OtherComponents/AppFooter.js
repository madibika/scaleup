import React from "react";
import styles from '../../assets/styles/OtherStyles/AppFooter.module.scss';
import Logo from '../../assets/icons/logoforblack.svg'


const AppFooter = () => {
    return(
        <div className={styles.footer_container}>
            <div className={styles.footer}>
                <div className={styles.logo_container}>
                    <img src={Logo} alt=""/>
                </div>
                <div className={styles.footer_items}>
                    <h3 className={styles.footer_items_title}>ПОЧТА:</h3>
                    <div className={styles.footer_items_value}>hello@scaleup.plus</div>
                </div>
                <div className={styles.footer_items}>
                    <h3 className={styles.footer_items_title}>АДРЕС:</h3>
                    <div className={styles.footer_items_value}>РФ, г. Москва, БЦ "Башня Федерации"</div>
                </div>
                <div className={styles.footer_items}>
                    <h3 className={styles.footer_items_title}>ТЕЛЕФОН:</h3>
                    <div className={styles.footer_items_value}>+7 499 350 27 07</div>
                </div>
            </div>
        </div>
    )
};

export default AppFooter;
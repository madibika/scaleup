import React from "react";
import styles from "../../assets/styles/FormStyles/FormProgress.module.scss";
import Runner from "../../assets/icons/runer.svg";
import ProgressLine from "./ProgressLine";
import {BigPlayButton, Player} from "video-react";

const FormProgress = () => {

    return(
        <div className={styles.progressContainer}>
            <div className={styles.container}>
                <div className={styles.title}>
                    <h3>Заполнено 0%</h3>
                </div>
                <div className={styles.progressLine}>
                    <ProgressLine/>
                </div>
                <div className={styles.body}>
                    <div className={styles.body_video}>
                        <Player playsInline fluid={false} width={230} height={100} src="https://media.w3.org/2010/05/sintel/trailer_hd.mp4">
                            <BigPlayButton width="50" position="center" />
                        </Player>
                    </div>
                    <div className={styles.body_description}>
                        Learn from a growing library of 1,982 websites and 3,829 component examples. Easily filterable to find the inspiration you need, quickly.
                    </div>
                </div>
            </div>
        </div>
    )
};

export default FormProgress;
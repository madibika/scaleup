import axios from 'axios'

axios.defaults.baseURL = 'http://192.168.1.24:8000/api';

axios.defaults.headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json'
};

export default axios


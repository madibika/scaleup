import React from 'react'
import { Route } from 'react-router-dom'
import LoginPage from "./LoginPage";
import RegistrationPage from "./RegistrationPage";
import AuthHeader from "../components/OtherComponents/AuthHeader";
import ForgottenPasswordPage from "./ForgottenPasswordPage";


const AuthContainer = () => {
    return(
        <>
            <AuthHeader/>
            <Route exact path={"/"} render={()=><LoginPage/>}/>
            <Route exact path={"/registration"} render={()=><RegistrationPage/>}/>
            <Route exact path={"/forgotten"} render={()=><ForgottenPasswordPage/>}/>
        </>
    )
};

export default AuthContainer
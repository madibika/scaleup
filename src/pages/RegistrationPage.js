import React, {useState} from 'react'
import styles from '../assets/styles/AuthStyles/RegistrationPage.module.scss'
import { useHistory } from "react-router-dom";
import TextField from "@material-ui/core/TextField";
import InputAdornment from '@material-ui/core/InputAdornment';
import Select from '@material-ui/core/Select';
import Flag from "../assets/icons/Flag.svg";
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import '../assets/styles/OtherStyles/TextField.scss';
import Check from '../assets/icons/check.svg'
import Warning from '../assets/icons/warning.svg'
import AuthService from "../services/AuthService";


const RegistrationPage = () => {
    const history = useHistory();
    const [isPending,setPending] = useState(false);
    const [form,setForm] = useState({
        fullName:'',
        companyName:'',
        phoneNumber:'',
        email:'',
        password:'',
        confirmPassword:''
    });
    const [focus,setFocus] = useState('');
    const [isChanged,setChanged] = useState({
        fullName:false,
        companyName:false,
        phoneNumber:false,
        email:false,
        password:false,
        confirmPassword:false
    });


    const handleChangeCountry = (e) => {
        const copyForm = {...form};
        copyForm.phoneNumber = e.target.value;
        setForm(copyForm)
    };

    const inputHandleChanged = event => {
        const copyForm = {...form};
        copyForm[event.target.name] = event.target.value;
        setForm(copyForm);
    };

    const submit = () => {
        setPending(true);
        const response = new AuthService().signUp(form.fullName,form.companyName,form.phoneNumber,form.email,form.password,form.confirmPassword);
        response.then(res=>{
           setPending(false);
           history.push('/')
        });
        response.catch(err=>{
            console.log(err.response.data.errors)
        });
    };

    return(
        <>
            <div className={styles.formContainer}>
                <div className={styles.form}>
                    <div className={styles.formTitle}>Регистрация</div>
                    <div className={styles.formSubtitle}>
                        <span className={styles.formSubtitle_question}>У вас есть аккаунт?</span>
                        <span onClick={()=>history.push('/')} className={styles.formSubtitle_answer}>Авторизуйтесь</span>
                        <div className={styles.formFields}>
                            <TextField
                                className={focus==='fullName' || form.fullName ? `${styles.textField} on` : `${styles.textField} off`}
                                // style={form.fullName.length>0 ? {backgroundColor:"#fff"} : {backgroundColor:"#f9fafa"}}
                                id="outlined-basic"
                                label="ФИО"
                                name="fullName"
                                value={form.fullName}
                                onChange={inputHandleChanged.bind(this)}
                                onFocus={()=>{
                                    setFocus('fullName');
                                    const copy = {...isChanged};
                                    copy.fullName = true;
                                    setChanged(copy);
                                }}
                                onBlur={()=>{
                                    setFocus('');
                                }}
                                variant="outlined"
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            {form.fullName ? (
                                                <img src={Check} alt=""/>
                                            ):(isChanged.fullName && (
                                                <img src={Warning} alt=""/>
                                            ))}
                                        </InputAdornment>
                                    ),
                                }}
                            />
                            <TextField
                                className={focus==='companyName' || form.companyName ? `${styles.textField} on` : `${styles.textField} off`}
                                id="outlined-basic"
                                label="Наименование компании"
                                name="companyName"
                                value={form.companyName}
                                onChange={inputHandleChanged.bind(this)}
                                onFocus={()=>{
                                    setFocus('companyName')
                                    const copy = {...isChanged};
                                    copy.companyName = true;
                                    setChanged(copy);
                                }}
                                onBlur={()=>setFocus('')}
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            {form.companyName ? (
                                                <img src={Check} alt=""/>
                                            ):(isChanged.companyName && (
                                                <img src={Warning} alt=""/>
                                            ))}
                                        </InputAdornment>
                                    ),
                                }}
                                variant="outlined"
                            />
                            <TextField
                                className={focus==='phoneNumber' || form.phoneNumber.length>2 ? `${styles.textField} on` : `${styles.textField} off`}
                                id="outlined-basic"
                                label=""
                                name="phoneNumber"
                                value={form.phoneNumber}
                                onChange={inputHandleChanged.bind(this)}
                                onFocus={()=>{
                                    setFocus('phoneNumber');
                                    const copy = {...isChanged};
                                    copy.phoneNumber = true;
                                    setChanged(copy);
                                }}
                                onBlur={()=>setFocus('')}
                                variant="outlined"
                                InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                            <Select
                                                labelId="demo-simple-select-label"
                                                id="demo-simple-select"
                                                onChange={handleChangeCountry}
                                            >
                                                <MenuItem style={{width:'64px'}} value={'+7'}>
                                                    <ListItemIcon>
                                                        <img src={Flag} />
                                                    </ListItemIcon>
                                                </MenuItem>
                                            </Select>
                                        </InputAdornment>
                                    ),
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            {form.phoneNumber.length===12 ? (
                                                <img src={Check} alt=""/>
                                            ):(isChanged.phoneNumber && (
                                                <img src={Warning} alt=""/>
                                            ))}
                                        </InputAdornment>
                                    ),
                                }}
                            />
                            <TextField
                                className={focus==='email' || form.email ? `${styles.textField} on` : `${styles.textField} off`}
                                id="outlined-basic"
                                label="Email"
                                name="email"
                                onChange={inputHandleChanged.bind(this)}
                                onFocus={()=>{
                                    setFocus('email');
                                    const copy = {...isChanged};
                                    copy.email = true;
                                    setChanged(copy);
                                }}
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            {form.email ? (
                                                <img src={Check} alt=""/>
                                            ):(isChanged.email && (
                                                <img src={Warning} alt=""/>
                                            ))}
                                        </InputAdornment>
                                    ),
                                }}
                                onBlur={()=>setFocus('')}
                                variant="outlined"
                            />
                            <TextField
                                className={focus==='password' || form.password ? `${styles.textField} on` : `${styles.textField} off`}
                                id="outlined-basic"
                                label="Придумайте пароль"
                                name="password"
                                type="password"
                                onChange={inputHandleChanged.bind(this)}
                                onFocus={()=>{
                                    setFocus('password');
                                    const copy = {...isChanged};
                                    copy.password = true;
                                    setChanged(copy);
                                }}
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            {form.password ? (
                                                <img src={Check} alt=""/>
                                            ):(isChanged.password && (
                                                <img src={Warning} alt=""/>
                                            ))}
                                        </InputAdornment>
                                    ),
                                }}
                                onBlur={()=>setFocus('')}
                                variant="outlined"
                            />
                            <TextField
                                error={isChanged.confirmPassword && (form.password !== form.confirmPassword)}
                                className={focus==='confirmPassword' || form.confirmPassword ? `${styles.textField} on` : `${styles.textField} off`}
                                id="outlined-basic"
                                label="Повторите пароль"
                                name="confirmPassword"
                                type="password"
                                onChange={inputHandleChanged.bind(this)}
                                onFocus={()=>{
                                    setFocus('confirmPassword')
                                    const copy = {...isChanged};
                                    copy.confirmPassword = true;
                                    setChanged(copy);
                                }}
                                onBlur={()=>setFocus('')}
                                variant="outlined"
                                helperText={isChanged.confirmPassword && (form.password !== form.confirmPassword) && "Пароли не совпадают."}
                            />
                        </div>
                        <div className={styles.btn}>
                            <button onClick={submit} className={isPending ? styles.signInBtnPending :styles.signInBtn} variant="contained" color="secondary">
                                Зарегистрироваться
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
};

export default RegistrationPage
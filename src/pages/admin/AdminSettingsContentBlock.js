import React, {useState} from "react";
import styles from "../../assets/styles/AdminStyles/AdminSettingsContentBlock.module.scss"
import ContentBlockItem from "../../components/AdminComponents/settings/ContentBlockItem";

const AdminSettingsContentBlock = () => {

    const [blockData] = useState([
        {
            title:'Приветсвенный блок',
            description:'Какое-то описание для этого блока',
            path:'/admin/settings/welcome'
        },
        {
            title:'Вопросы-ответы',
            description:'Какое-то описание для этого блока',
            path:'/admin/settings/faqs'
        },
        {
            title:'Стратегический раздел',
            description:'Какое-то описание для этого блока',
            path:'/admin'
        },
        {
            title:'Финансовый раздел',
            description:'Какое-то описание для этого блока',
            path:'/admin'
        },
        {
            title:'Маркетинговый раздел',
            description:'Какое-то описание для этого блока',
            path:'/admin'
        },
        {
            title:'Юридический раздел',
            description:'Какое-то описание для этого блока',
            path:'/admin'
        }
    ]);

    return(
        <div className={styles.container}>
            <div className={styles.content}>
                {blockData.map((el,index)=>(
                    <ContentBlockItem
                        key={index}
                        title={el.title}
                        description={el.description}
                        path={el.path}
                    />
                ))}
            </div>
        </div>
    )
};

export default AdminSettingsContentBlock;
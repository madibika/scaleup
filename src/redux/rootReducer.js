import {combineReducers} from "redux";
import {AuthReducer} from "./reducers/AuthReducer";
import {MainReducer} from "./reducers/MainReducer";
import {AdminReducer} from "./reducers/AdminReducer";
import {QuestionnaireReducer} from "./reducers/QuestionnaireReducer";
import {SettingsReducer} from "./reducers/SettingsReducer";


export const rootReducer = combineReducers({
    AuthPage: AuthReducer,
    MainPage: MainReducer,
    AdminPage: AdminReducer,
    QuestionnairePage: QuestionnaireReducer,
    SettingsPage: SettingsReducer
});
import React, {useEffect, useState} from "react";
import styles from "../../assets/styles/AdminStyles/AdminSettingsPage.module.scss"
import AdminTitle from "../../components/AdminComponents/AdminTitle";
import AdminSettingsTabs from "../../components/AdminComponents/settings/AdminSettingsTabs";
import AdminSettingsTable from "../../components/AdminComponents/settings/AdminSettingsTable";
import {useDispatch, useSelector} from "react-redux";
import {getWorkersAction} from "../../redux/actions/settings/getWorkersAction";
import {getServicesAction} from "../../redux/actions/settings/getServicesAction";
import {getUsersAction} from "../../redux/actions/settings/getUsersAction";
import AdminSettingsContentBlock from "./AdminSettingsContentBlock";
import {getSectionsAction} from "../../redux/actions/settings/getSectionsAction";

const AdminSettingsPage = () => {
    const dispatch = useDispatch();
    const [active,setActive] = useState(1);
    const {workers,services,users_list} = useSelector(state => state.SettingsPage);

    useEffect(()=>{
        dispatch(getWorkersAction());
        dispatch(getServicesAction());
        dispatch(getUsersAction());
        dispatch(getSectionsAction());
    },[]);

    return(
        <div className={styles.container}>
            <AdminTitle
                title={"Настройки"}
                description={"Админская панель"}
                count={""}
            />
            <AdminSettingsTabs
                active={active}
                setActive={setActive}
            />
            {(active === 1 && workers) && (
                <AdminSettingsTable
                    active={active}
                    data={workers}
                    count={workers.length}
                />
            )}
            {(active === 2 &&  services ) && (
                <AdminSettingsTable
                    active={active}
                    data={services}
                    count={services.length}
                />
            )}
            {active === 3 && (
                <AdminSettingsContentBlock

                />
            )}
            {(active === 5 && users_list) && (
                <AdminSettingsTable
                    active={active}
                    data={users_list}
                    count={users_list.length}
                />
            )}
        </div>
    )
};
export default AdminSettingsPage;
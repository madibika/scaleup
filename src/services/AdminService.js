import axios from '../plugins/axios'

export default class AdminService {

    async postResource(url,payload){
        return await axios.post(url,payload)
    }
    async getResource(url){
       return await axios.get(url)
    }
    async getPaymentsTableData(){
        return await this.getResource('/payments/list')
    }
    async agreePayment(userId){
        return await this.getResource(`/payments/accepted/${userId}`)
    }
    async getQuestionnaireTable(){
        return await this.getResource('/worksheets')
    }
    async getCurrentQuestionnaireItem(id){
        return await this.getResource(`/payments/show/${id}`)
    }

}
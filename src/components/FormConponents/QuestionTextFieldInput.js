import React, {useEffect, useState} from "react";
import PropTypes from 'prop-types';
import styles from '../../assets/styles/FormStyles/QuestionTextFieldInput.module.scss';
import TextField from "@material-ui/core/TextField/TextField";
import AnketaService from "../../services/AnketaService";
import InputAdornment from "@material-ui/core/InputAdornment";
import Comment from "../../assets/icons/comment.svg"

const QuestionTextFieldInput = ({id,title="",text="",placeholder="",require=false,questionsData,getStrategyQuestion,setOpen,setInfo,commentOpen,commentInfo}) => {
    const [focus,setFocus] = useState(false);
    const [form,setForm] = useState(questionsData);
    const [error,setError] = useState(form.answer ? (form.answer.commentary ? (form.answer.commentary.read === 0 ? true : false) : false) : false);

    const sendAnswer = () => {
        if (form.answer){
            const response = new AnketaService().saveForm(id,form.answer);
            response.then(res=>{
                getStrategyQuestion()
            })
        }
    };
    const inputHandleChanged = e => {
        if (questionsData.type === 1){
            const copy = {...form};
            copy.answer = {...form.answer};
            copy.answer.answers = {
                '1': e.target.value
            };
            setForm(copy);
        }
    };

    useEffect(()=>{
        setForm(questionsData);
        let a = false;
        if (questionsData.answer){
            if (questionsData.answer.commentary){
                if (questionsData.answer.commentary.read === 0){
                    setError(true);
                    a = true;
                    console.log('error')
                }
            }
        }
        if (!a){
            setError(false);
            console.log("checked")
        }
    },[questionsData]);

    return(
        <div style={{flex:1}} className={styles.root}>
                {title && (
                    <div className={styles.title}>
                        {title}
                    </div>
                )}
                <div className={styles.text}>
                    {text} {require && (
                    <span>*</span>
                )}
                </div>
            {form && (
                <div className={styles.input}>
                    <TextField
                        className={error ?  `${styles.textField} warning` : ( focus || form.answer ? `${styles.textField} on` : `${styles.textField} off`)}
                        id="outlined-basic"
                        placeholder={placeholder}
                        name="email"
                        variant="outlined"
                        disabled={form ? (form.answer ? (form.answer.disabled === 1 ) : false) : false}
                        style={error ? {borderColor:'#FF991F'} : {}}
                        value={form.answer ? form.answer.answers['1'] : ""}
                        onFocus={()=>{
                            setFocus(true);
                        }}
                        onBlur={()=>{
                            setFocus('');
                            sendAnswer();
                        }}
                        onChange={(e)=>{
                            inputHandleChanged(e);
                        }}
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    {error && (
                                        <img
                                            onClick={()=>{
                                                if (commentInfo){
                                                    if (commentInfo.answer_id === form.answer.commentary.answer_id){
                                                        setOpen(!commentOpen);
                                                    }else{
                                                        setInfo(form.answer.commentary);
                                                    }
                                                }else{
                                                    setOpen(!commentOpen);
                                                    setInfo(form.answer.commentary);
                                                }
                                            }}
                                            className={styles.comment}
                                            src={Comment} alt=""/>
                                    )}
                                </InputAdornment>
                            ),
                        }}
                    />
                </div>
            )}
        </div>
    )
};

QuestionTextFieldInput.propTypes = {
    title: PropTypes.string,
    text: PropTypes.string,
    require: PropTypes.bool
};



export default QuestionTextFieldInput;
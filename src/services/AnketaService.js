import axios from "../plugins/axios"

export default class AnketaService{

    async postResource(url,payload,config=null){
        if (config){
            return await axios.post(url,payload,config);
        }else{
            return await axios.post(url,payload);
        }
    }

    async putResource(url,payload){
        return await axios.put(url,payload);
    }

    async saveForm(id,answer,type=1){
        if (type === 3){
            const config = {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Accept': 'application/json'
                }
            };
            const formData = new FormData();
            let checkArr = [0,0];
            formData.append('form_id',id);
            formData.append('type',type);
            for (let i=0;i<answer.length;i++){
                if (answer[i].slice(0,7) === "answers"){
                    checkArr[0] = 1;
                    formData.append('repeated[]',answer[i]);
                }else{
                    checkArr[1] = 1;
                    formData.append('answers[]',answer[i]);
                }
            }
            if (checkArr[0] === 0){
                formData.append('repeated','empty')
            }
            if (checkArr[1] === 0){
                formData.append('answers','empty');
            }

            return await this.postResource('/answers',formData,config);
        }
        if (type === 1){
            let obj = answer.answers;
            let bool = true;
            for (const [key, value] of Object.entries(answer.answers)) {
                if (value === ""){
                    bool = false;
                    break;
                }
            }
            let payload = {};
            if (!bool){
                payload = {
                    form_id: id,
                    type:type,
                    answers:'empty',
                    repeated: 'empty'
                };
            }else{
                payload = {
                    form_id: id,
                    type:type,
                    answers:obj,
                    repeated: 'empty'
                };
            }
            return await this.postResource('/answers',payload);
        }
        if (type === 4){
            let payload = {};
            if (Object.keys(answer).length>0){
                payload = {
                    form_id: id,
                    answers: answer,
                    type: type,
                    repeated: "empty"
                };
            }else{
                payload = {
                    form_id: id,
                    answers: "empty",
                    type: type,
                    repeated: "empty"
                };
            }
            return await this.postResource('/answers',payload);
        }
    }
    async commentReadIt(id){
        return await this.postResource(`/commentary/${id}/read`)
    }
    async saveAnketa(title){
        return await this.putResource('/worksheets/saved',{type:title})
    }
    async worksheetsAPI(title){
        return await this.putResource('/worksheets/pending',{type:title});
    }
}
import React, {useEffect, useState} from "react";
import styles from "../../assets/styles/FormStyles/QuestionRadioInput.module.scss"
import AnketaService from "../../services/AnketaService";

const QuestionRadioInput = ({id, title, text, require, questionsData,getStrategyQuestion}) => {
    const [answers,setAnswers] = useState(null);
    const [formDisabled,setFormDisabled] = useState(true);
    const [formAnswer,setFormAnswer] = useState('');

    useEffect(()=>{
        let obj = {};
        let drugoe = "";
        for (const [key, value] of Object.entries(questionsData.answers)) {
            obj[key] = {
                [key]: value,
                checked : false
            };
        }
        if (questionsData.answer){
            let str = "ans-";
            for (const [key, value] of Object.entries(questionsData.answer.answers)) {
                console.log(str+key);
                console.log(drugoe);
                if ((str+key) === drugoe){
                    setFormAnswer(value);
                    setFormDisabled(false);
                    obj[str+key] = {
                        [str+key]: "Другое:",
                        checked : true
                    }
                }else{
                    obj[str+key] = {
                        [str+key]: value,
                        checked : true
                    };
                }
            }
        }
        setAnswers(obj);

    },[questionsData]);

    const handleChange = (name) => {
        const copy = {...answers};
        for (const [key, value] of Object.entries(copy)){
            if (key === name){
                copy[key].checked = true;
            }else{
                copy[key].checked = false;
            }
        }
        console.log(copy);
        setAnswers(copy);
        let obj = {};
        let i=1;
        for (const [key, value] of Object.entries(copy)) {
            if (copy[key].checked){
                obj[i] = value[key];
            }
            i++;
        }
        const response = new AnketaService().saveForm(id,obj,4);
        response.then(res=>{
            console.log(res);
            getStrategyQuestion();
        })
    };

    return (
        <div className={styles.root}>
            {title && (
                <div className={styles.title}>
                    {title}
                </div>
            )}
            <div className={styles.text}>
                {text} {require && (
                <span>*</span>
            )}
            </div>
            <div className={styles.input}>
                {answers && (
                    <>
                        {Object.keys(answers).map((key,index)=>(
                            <div key={index} className={styles.checkbox}>
                                <input
                                    disabled={questionsData ? (questionsData.answer ? (questionsData.answer.disabled === 1 ) : false) : false}
                                    checked={answers[key].checked} onChange={(e)=>handleChange(key)} type="radio" name={key}/>
                                <label htmlFor="vehicle1">{answers[key][key]}</label><br/>
                            </div>
                        ))}
                    </>
                )}
            </div>
        </div>
    )
};
export default QuestionRadioInput


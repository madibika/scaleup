import React, {useEffect, useState} from "react";
import AdminTitle from "../../components/AdminComponents/AdminTitle";
import AdminTabs from "../../components/AdminComponents/AdminTabs";
import PaymentTable from "../../components/AdminComponents/PaymentTable";
import {getPaymentTableAction} from "../../redux/actions/getPaymentTableAction";
import {useDispatch, useSelector} from "react-redux";
import Modal from "react-modal";
import PaymentInfoModal from "../../components/AdminComponents/PaymentInfoModal";

const customStyles = {
    content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)',
        borderRadius          : '32px',
        padding               : 0
    }
};

const AdminPage = () => {
    const dispatch = useDispatch();
    const [modalIsOpen,setOpen] = useState(false);
    const {card_paid_data,invoice_paid_data,dont_paid_data,status} = useSelector(state => state.AdminPage);
    const [active,setActive] = useState(1);
    const [info,setInfo] = useState(null);

    useEffect(()=>{
        dispatch(getPaymentTableAction())
    },[]);

    const openModal = (info) => {
        setInfo(info);
        setOpen(true);
    };

    const closeModal = () => {
        setOpen(false)
    };


    return (
        <>
            <Modal
                isOpen={modalIsOpen}
                onRequestClose={closeModal}
                style={customStyles}
                contentLabel="Example Modal"
            >
                <PaymentInfoModal
                    info={info}
                    closeModal={closeModal}
                />
            </Modal>
            <div style={{backgroundColor: "#f9f9f9",paddingTop:20, height: '100vh'}}>
                <AdminTitle
                    title={"Оплата"}
                    description={"Общее количество компании:"}
                    count={card_paid_data.length+invoice_paid_data.length+dont_paid_data.length}
                />
                <AdminTabs
                    card_paid_count={card_paid_data.length}
                    invoice_paid_count={invoice_paid_data.length}
                    dont_paid_count={dont_paid_data.length}
                    setActive={setActive}
                    active={active}
                />
                <PaymentTable
                    openModal={openModal}
                    active={active}
                    status={status}
                    dont_paid_data={dont_paid_data}
                    invoice_paid_data={invoice_paid_data}
                    card_paid_data={card_paid_data}
                />
            </div>
        </>
    )
};

export default AdminPage;
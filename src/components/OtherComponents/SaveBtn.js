import React from "react";
import PropTypes, {bool} from 'prop-types';
import styles from "../../assets/styles/OtherStyles/SaveBtn.module.scss";

const SaveBtn = ({ title = "",disabled,save}) => {
    console.log(disabled);
    return(
        <>
            <div onClick={save} style={disabled ? {} : {opacity:'.6'}} className={styles.saveBtn}>
                { title }
            </div>
        </>
    )
};

SaveBtn.propTypes = {
    title : PropTypes.string
};

export default SaveBtn
import './App.css';
import React, {useEffect, useState} from 'react'
import AuthContainer from "./pages/AuthContainer";
import AppContainer from "./pages/AppContainer";
import {useDispatch, useSelector} from "react-redux";
import {getMeInfoAction} from "./redux/actions/getMeInfo";
import axios from './plugins/axios'
import {useHistory} from 'react-router-dom'

function App() {
  const dispatch = useDispatch();
  const {userData} = useSelector(state => state.AuthPage);
  const history = useHistory();


  useEffect(()=>{
    if (localStorage.getItem('token')){
      axios.defaults.headers = {
          Authorization: `Bearer ${localStorage.getItem('token')}`
      };
      const response = dispatch(getMeInfoAction(localStorage.getItem('token')));
      response.then(res=>{
        console.log(res.data);
        if (res.data.payment && res.data.roles[0].name === "client"){
            if (res.data.payment.payment_status_id ===1){
              history.push('/payment')
            }
        }
      });
      response.catch(err=>{
        console.log(err.response)
      })
    }
  },[]);

  if(!userData && !localStorage.getItem('token')){
    return (
        <AuthContainer/>
    )
  }
    return (
        <div className="App">
          <AppContainer userData={userData}/>
        </div>
    );
}

export default App;
